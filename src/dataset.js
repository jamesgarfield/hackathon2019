const datasets = {
  Sprint: [
    {
      service_id: "12",
      at_reach_pct: 36.211830760476204,
      a18_plus_reach_pct: 18.8891347174103,
      display_name: "ESPN",
      p2_plus_reach_pct: 15.11130777392824,
      at_imps: 8774004393,
      trps: 19710.064939745174,
      a18_imps: 41522870903,
      a18_grps: 17072.8468825295,
      p2_imps: 45442607779,
      p2_grps: 14787.70184803124,
      index_over_a18: 115,
      index_over_p2: 133
    },
    {
      service_id: "38",
      at_reach_pct: 14.863226701151099,
      a18_plus_reach_pct: 12.775536203381698,
      display_name: "ESPN2",
      p2_plus_reach_pct: 10.60369504880681,
      at_imps: 2271738655,
      trps: 5103.2703437270175,
      a18_imps: 12695826044,
      a18_grps: 5220.108566259611,
      p2_imps: 13873247134,
      p2_grps: 4514.561384315002,
      index_over_a18: 98,
      index_over_p2: 113
    },
    {
      service_id: "40",
      at_reach_pct: 29.056846559836195,
      a18_plus_reach_pct: 21.7237651744362,
      display_name: "FX",
      p2_plus_reach_pct: 20.63757691571439,
      at_imps: 311566947,
      trps: 699.909012413732,
      a18_imps: 1883159561,
      a18_grps: 774.2936396529748,
      p2_imps: 2117412910,
      p2_grps: 689.037718841523,
      index_over_a18: 90,
      index_over_p2: 102
    },
    {
      service_id: "50",
      at_reach_pct: 6.84575673922262,
      a18_plus_reach_pct: 5.1462183653984495,
      display_name: "FX Movie Channel",
      p2_plus_reach_pct: 4.425747794242667,
      at_imps: 38173030,
      trps: 85.75250950684014,
      a18_imps: 254055256,
      a18_grps: 104.45921467044941,
      p2_imps: 281787324,
      p2_grps: 91.6977949886105,
      index_over_a18: 82,
      index_over_p2: 94
    },
    {
      service_id: "63",
      at_reach_pct: 4.05048255906519,
      a18_plus_reach_pct: 3.53129019093374,
      display_name: "ESPNews",
      p2_plus_reach_pct: 2.648467643200305,
      at_imps: 70424549,
      trps: 158.20284218522653,
      a18_imps: 345820296,
      a18_grps: 142.18999876649806,
      p2_imps: 420832512,
      p2_grps: 136.9451714936544,
      index_over_a18: 111,
      index_over_p2: 116
    },
    {
      service_id: "70",
      at_reach_pct: 15.233021173347002,
      a18_plus_reach_pct: 12.4427352944826,
      display_name: "FXX",
      p2_plus_reach_pct: 10.203042941475733,
      at_imps: 171341693,
      trps: 384.9047423487297,
      a18_imps: 945750295,
      a18_grps: 388.8615990296452,
      p2_imps: 1081317971,
      p2_grps: 351.8769837292548,
      index_over_a18: 99,
      index_over_p2: 109
    },
    {
      service_id: "87",
      at_reach_pct: 14.900052937634301,
      a18_plus_reach_pct: 11.347775820931401,
      display_name: "National Geographic",
      p2_plus_reach_pct: 9.30517617316375,
      at_imps: 121154788,
      trps: 272.164068394679,
      a18_imps: 1293308296,
      a18_grps: 531.7660852760989,
      p2_imps: 1364652816,
      p2_grps: 444.07836511552233,
      index_over_a18: 51,
      index_over_p2: 61
    },
    {
      service_id: "106",
      at_reach_pct: 4.25967072107857,
      a18_plus_reach_pct: 4.246523038639061,
      display_name: "ESPNU",
      p2_plus_reach_pct: 3.48214889168403,
      at_imps: 87618916,
      trps: 196.82854498041104,
      a18_imps: 614715062,
      a18_grps: 252.7507347559722,
      p2_imps: 702538378,
      p2_grps: 228.61645883501467,
      index_over_a18: 78,
      index_over_p2: 86
    },
    {
      service_id: "120",
      at_reach_pct: 5.957072198499691,
      a18_plus_reach_pct: 3.86590398914536,
      display_name: "Nat Geo Wild",
      p2_plus_reach_pct: 3.6726087896880926,
      at_imps: 59594484,
      trps: 133.87401065063028,
      a18_imps: 835538092,
      a18_grps: 343.5459446568809,
      p2_imps: 895181924,
      p2_grps: 291.3055398633257,
      index_over_a18: 39,
      index_over_p2: 46
    },
    {
      service_id: "2074",
      at_reach_pct: 21.345916287555802,
      a18_plus_reach_pct: 16.3022207928102,
      display_name: "Freeform",
      p2_plus_reach_pct: 19.073598327587934,
      at_imps: 306043081,
      trps: 687.5001125221057,
      a18_imps: 1196239328,
      a18_grps: 491.8544994038073,
      p2_imps: 1505934241,
      p2_grps: 490.05344646924823,
      index_over_a18: 140,
      index_over_p2: 140
    }
  ],
  "NY Life": [
    {
      service_id: "12",
      at_real_reach_pct: "36.211830760476204",
      a18_plus_reach_pct: 18.8891347174103,
      display_name: "ESPN",
      p2_plus_reach_pct: 15.11130777392824,
      at_imps: 4492248729,
      trps: 20244.28765375257,
      a18_imps: 41522870903,
      a18_grps: 17072.8468825295,
      p2_imps: 45442607779,
      p2_grps: 14787.70184803124,
      index_over_a18: 119,
      index_over_p2: 137,
      at_reach_pct: 27.158873070357153
    },
    {
      service_id: "38",
      at_real_reach_pct: "14.863226701151099",
      a18_plus_reach_pct: 12.775536203381698,
      display_name: "ESPN2",
      p2_plus_reach_pct: 10.60369504880681,
      at_imps: 1101762590,
      trps: 4965.085449994119,
      a18_imps: 12695826044,
      a18_grps: 5220.108566259611,
      p2_imps: 13873247134,
      p2_grps: 4514.561384315002,
      index_over_a18: 95,
      index_over_p2: 110,
      at_reach_pct: 13.37690403103599
    },
    {
      service_id: "40",
      at_real_reach_pct: "29.056846559836195",
      a18_plus_reach_pct: 21.7237651744362,
      display_name: "FX",
      p2_plus_reach_pct: 20.63757691571439,
      at_imps: 166971889,
      trps: 752.4576583945851,
      a18_imps: 1883159561,
      a18_grps: 774.2936396529748,
      p2_imps: 2117412910,
      p2_grps: 689.037718841523,
      index_over_a18: 97,
      index_over_p2: 109,
      at_reach_pct: 24.988888041459127
    },
    {
      service_id: "50",
      at_real_reach_pct: "6.84575673922262",
      a18_plus_reach_pct: 5.1462183653984495,
      display_name: "FX Movie Channel",
      p2_plus_reach_pct: 4.425747794242667,
      at_imps: 19396285,
      trps: 87.40922374144526,
      a18_imps: 254055256,
      a18_grps: 104.45921467044941,
      p2_imps: 281787324,
      p2_grps: 91.6977949886105,
      index_over_a18: 84,
      index_over_p2: 95,
      at_reach_pct: 5.134317554416965
    },
    {
      service_id: "63",
      at_real_reach_pct: "4.05048255906519",
      a18_plus_reach_pct: 3.53129019093374,
      display_name: "ESPNews",
      p2_plus_reach_pct: 2.648467643200305,
      at_imps: 23446525,
      trps: 105.66160146052763,
      a18_imps: 345820296,
      a18_grps: 142.18999876649806,
      p2_imps: 420832512,
      p2_grps: 136.9451714936544,
      index_over_a18: 74,
      index_over_p2: 77,
      at_reach_pct: 3.0783667448895446
    },
    {
      service_id: "70",
      at_real_reach_pct: "15.233021173347002",
      a18_plus_reach_pct: 12.4427352944826,
      display_name: "FXX",
      p2_plus_reach_pct: 10.203042941475733,
      at_imps: 84707718,
      trps: 381.73473985787456,
      a18_imps: 945750295,
      a18_grps: 388.8615990296452,
      p2_imps: 1081317971,
      p2_grps: 351.8769837292548,
      index_over_a18: 98,
      index_over_p2: 108,
      at_reach_pct: 15.842342020280881
    },
    {
      service_id: "87",
      at_real_reach_pct: "14.900052937634301",
      a18_plus_reach_pct: 11.347775820931401,
      display_name: "National Geographic",
      p2_plus_reach_pct: 9.30517617316375,
      at_imps: 86677062,
      trps: 390.6095791003045,
      a18_imps: 1293308296,
      a18_grps: 531.7660852760989,
      p2_imps: 1364652816,
      p2_grps: 444.07836511552233,
      index_over_a18: 73,
      index_over_p2: 88,
      at_reach_pct: 13.410047643870872
    },
    {
      service_id: "106",
      at_real_reach_pct: "4.25967072107857",
      a18_plus_reach_pct: 4.246523038639061,
      display_name: "ESPNU",
      p2_plus_reach_pct: 3.48214889168403,
      at_imps: 52126941,
      trps: 234.909695885024,
      a18_imps: 614715062,
      a18_grps: 252.7507347559722,
      p2_imps: 702538378,
      p2_grps: 228.61645883501467,
      index_over_a18: 93,
      index_over_p2: 103,
      at_reach_pct: 3.1521563335981417
    },
    {
      service_id: "120",
      at_real_reach_pct: "5.957072198499691",
      a18_plus_reach_pct: 3.86590398914536,
      display_name: "Nat Geo Wild",
      p2_plus_reach_pct: 3.6726087896880926,
      at_imps: 50977817,
      trps: 229.7311765074717,
      a18_imps: 835538092,
      a18_grps: 343.5459446568809,
      p2_imps: 895181924,
      p2_grps: 291.3055398633257,
      index_over_a18: 67,
      index_over_p2: 79,
      at_reach_pct: 6.493208696364663
    },
    {
      service_id: "2074",
      at_real_reach_pct: "21.345916287555802",
      a18_plus_reach_pct: 16.3022207928102,
      display_name: "Freeform",
      p2_plus_reach_pct: 19.073598327587934,
      at_imps: 125718063,
      trps: 566.5475762330092,
      a18_imps: 1196239328,
      a18_grps: 491.8544994038073,
      p2_imps: 1505934241,
      p2_grps: 490.05344646924823,
      index_over_a18: 115,
      index_over_p2: 116,
      at_reach_pct: 17.076733030044643
    }
  ],
  "ATSG QSR Influential": [
    {
      service_id: "12",
      at_real_reach_pct: "36.211830760476204",
      a18_plus_reach_pct: 18.8891347174103,
      display_name: "ESPN",
      p2_plus_reach_pct: 15.11130777392824,
      at_imps: 4501219429,
      trps: 21244.743606743566,
      a18_imps: 47154090357,
      a18_grps: 19388.220203527813,
      p2_imps: 51544369167,
      p2_grps: 16773.30594435405,
      index_over_a18: 110,
      index_over_p2: 127,
      at_reach_pct: 38.74665891370954
    },
    {
      service_id: "38",
      at_real_reach_pct: "14.863226701151099",
      a18_plus_reach_pct: 12.775536203381698,
      display_name: "ESPN2",
      p2_plus_reach_pct: 10.60369504880681,
      at_imps: 1373304557,
      trps: 6481.688719157741,
      a18_imps: 15394221152,
      a18_grps: 6329.600407877966,
      p2_imps: 16828513577,
      p2_grps: 5476.249130165961,
      index_over_a18: 102,
      index_over_p2: 118,
      at_reach_pct: 13.37690403103599
    },
    {
      service_id: "40",
      at_real_reach_pct: "29.056846559836195",
      a18_plus_reach_pct: 21.7237651744362,
      display_name: "FX",
      p2_plus_reach_pct: 20.63757691571439,
      at_imps: 165617708,
      trps: 781.6783438313352,
      a18_imps: 1883159561,
      a18_grps: 774.2936396529748,
      p2_imps: 2117412910,
      p2_grps: 689.037718841523,
      index_over_a18: 101,
      index_over_p2: 113,
      at_reach_pct: 27.022867300647665
    },
    {
      service_id: "50",
      at_real_reach_pct: "6.84575673922262",
      a18_plus_reach_pct: 5.1462183653984495,
      display_name: "FX Movie Channel",
      p2_plus_reach_pct: 4.425747794242667,
      at_imps: 21275909,
      trps: 100.41750498229663,
      a18_imps: 254055256,
      a18_grps: 104.45921467044941,
      p2_imps: 281787324,
      p2_grps: 91.6977949886105,
      index_over_a18: 96,
      index_over_p2: 110,
      at_reach_pct: 6.229638632692584
    },
    {
      service_id: "63",
      at_real_reach_pct: "4.05048255906519",
      a18_plus_reach_pct: 3.53129019093374,
      display_name: "ESPNews",
      p2_plus_reach_pct: 2.648467643200305,
      at_imps: 44718702,
      trps: 211.0622199171179,
      a18_imps: 432275370,
      a18_grps: 177.7374984581226,
      p2_imps: 526040640,
      p2_grps: 171.181464367068,
      index_over_a18: 119,
      index_over_p2: 123,
      at_reach_pct: 2.9973570937082408
    },
    {
      service_id: "70",
      at_real_reach_pct: "15.233021173347002",
      a18_plus_reach_pct: 12.4427352944826,
      display_name: "FXX",
      p2_plus_reach_pct: 10.203042941475733,
      at_imps: 81790755,
      trps: 386.03397249418816,
      a18_imps: 945750295,
      a18_grps: 388.8615990296452,
      p2_imps: 1081317971,
      p2_grps: 351.8769837292548,
      index_over_a18: 99,
      index_over_p2: 110,
      at_reach_pct: 17.67030456108252
    },
    {
      service_id: "87",
      at_real_reach_pct: "14.900052937634301",
      a18_plus_reach_pct: 11.347775820931401,
      display_name: "National Geographic",
      p2_plus_reach_pct: 9.30517617316375,
      at_imps: 90765617,
      trps: 428.39330265488957,
      a18_imps: 1293308296,
      a18_grps: 531.7660852760989,
      p2_imps: 1364652816,
      p2_grps: 444.07836511552233,
      index_over_a18: 81,
      index_over_p2: 96,
      at_reach_pct: 12.069042879483785
    },
    {
      service_id: "106",
      at_real_reach_pct: "4.25967072107857",
      a18_plus_reach_pct: 4.246523038639061,
      display_name: "ESPNU",
      p2_plus_reach_pct: 3.48214889168403,
      at_imps: 40215286,
      trps: 189.80710620092418,
      a18_imps: 700643926,
      a18_grps: 288.08187410057155,
      p2_imps: 801315420,
      p2_grps: 260.75998047510575,
      index_over_a18: 66,
      index_over_p2: 73,
      at_reach_pct: 4.046687185024642
    },
    {
      service_id: "120",
      at_real_reach_pct: "5.957072198499691",
      a18_plus_reach_pct: 3.86590398914536,
      display_name: "Nat Geo Wild",
      p2_plus_reach_pct: 3.6726087896880926,
      at_imps: 69331922,
      trps: 327.23108364443317,
      a18_imps: 835538092,
      a18_grps: 343.5459446568809,
      p2_imps: 895181924,
      p2_grps: 291.3055398633257,
      index_over_a18: 95,
      index_over_p2: 112,
      at_reach_pct: 7.6250524140796045
    },
    {
      service_id: "2074",
      at_real_reach_pct: "21.345916287555802",
      a18_plus_reach_pct: 16.3022207928102,
      display_name: "Freeform",
      p2_plus_reach_pct: 19.073598327587934,
      at_imps: 127138845,
      trps: 600.0667632031349,
      a18_imps: 1196239328,
      a18_grps: 491.8544994038073,
      p2_imps: 1505934241,
      p2_grps: 490.05344646924823,
      index_over_a18: 122,
      index_over_p2: 122,
      at_reach_pct: 20.705538798929126
    }
  ],
  "ATSG Auto Insurance Switchers": [
    {
      service_id: "12",
      at_real_reach_pct: "36.211830760476204",
      a18_plus_reach_pct: 18.8891347174103,
      display_name: "ESPN",
      p2_plus_reach_pct: 15.11130777392824,
      at_imps: 6923314467,
      trps: 19790.03412223849,
      a18_imps: 47154090357,
      a18_grps: 19388.220203527813,
      p2_imps: 51544369167,
      p2_grps: 16773.30594435405,
      index_over_a18: 102,
      index_over_p2: 118,
      at_reach_pct: 45.264788450595255
    },
    {
      service_id: "38",
      at_real_reach_pct: "14.863226701151099",
      a18_plus_reach_pct: 12.775536203381698,
      display_name: "ESPN2",
      p2_plus_reach_pct: 10.60369504880681,
      at_imps: 2124053279,
      trps: 6071.526443776559,
      a18_imps: 15394221152,
      a18_grps: 6329.600407877966,
      p2_imps: 16828513577,
      p2_grps: 5476.249130165961,
      index_over_a18: 96,
      index_over_p2: 111,
      at_reach_pct: 17.092710706323764
    },
    {
      service_id: "40",
      at_real_reach_pct: "29.056846559836195",
      a18_plus_reach_pct: 21.7237651744362,
      display_name: "FX",
      p2_plus_reach_pct: 20.63757691571439,
      at_imps: 246453764,
      trps: 704.478818055165,
      a18_imps: 1883159561,
      a18_grps: 774.2936396529748,
      p2_imps: 2117412910,
      p2_grps: 689.037718841523,
      index_over_a18: 91,
      index_over_p2: 102,
      at_reach_pct: 36.030489734196884
    },
    {
      service_id: "50",
      at_real_reach_pct: "6.84575673922262",
      a18_plus_reach_pct: 5.1462183653984495,
      display_name: "FX Movie Channel",
      p2_plus_reach_pct: 4.425747794242667,
      at_imps: 32600922,
      trps: 93.1885085521366,
      a18_imps: 254055256,
      a18_grps: 104.45921467044941,
      p2_imps: 281787324,
      p2_grps: 91.6977949886105,
      index_over_a18: 89,
      index_over_p2: 102,
      at_reach_pct: 5.065859987024739
    },
    {
      service_id: "63",
      at_real_reach_pct: "4.05048255906519",
      a18_plus_reach_pct: 3.53129019093374,
      display_name: "ESPNews",
      p2_plus_reach_pct: 2.648467643200305,
      at_imps: 66369145,
      trps: 189.7137046045409,
      a18_imps: 432275370,
      a18_grps: 177.7374984581226,
      p2_imps: 526040640,
      p2_grps: 171.181464367068,
      index_over_a18: 107,
      index_over_p2: 111,
      at_reach_pct: 3.5644246519773675
    },
    {
      service_id: "70",
      at_real_reach_pct: "15.233021173347002",
      a18_plus_reach_pct: 12.4427352944826,
      display_name: "FXX",
      p2_plus_reach_pct: 10.203042941475733,
      at_imps: 134664335,
      trps: 384.9329386876759,
      a18_imps: 945750295,
      a18_grps: 388.8615990296452,
      p2_imps: 1081317971,
      p2_grps: 351.8769837292548,
      index_over_a18: 99,
      index_over_p2: 109,
      at_reach_pct: 18.43195561974987
    },
    {
      service_id: "87",
      at_real_reach_pct: "14.900052937634301",
      a18_plus_reach_pct: 11.347775820931401,
      display_name: "National Geographic",
      p2_plus_reach_pct: 9.30517617316375,
      at_imps: 168831153,
      trps: 482.5975045053129,
      a18_imps: 1293308296,
      a18_grps: 531.7660852760989,
      p2_imps: 1364652816,
      p2_grps: 444.07836511552233,
      index_over_a18: 91,
      index_over_p2: 109,
      at_reach_pct: 13.112046585118184
    },
    {
      service_id: "106",
      at_real_reach_pct: "4.25967072107857",
      a18_plus_reach_pct: 4.246523038639061,
      display_name: "ESPNU",
      p2_plus_reach_pct: 3.48214889168403,
      at_imps: 72603180,
      trps: 207.5334606436601,
      a18_imps: 700643926,
      a18_grps: 288.08187410057155,
      p2_imps: 801315420,
      p2_grps: 260.75998047510575,
      index_over_a18: 72,
      index_over_p2: 80,
      at_reach_pct: 5.15420157250507
    },
    {
      service_id: "120",
      at_real_reach_pct: "5.957072198499691",
      a18_plus_reach_pct: 3.86590398914536,
      display_name: "Nat Geo Wild",
      p2_plus_reach_pct: 3.6726087896880926,
      at_imps: 96268166,
      trps: 275.17893200712507,
      a18_imps: 835538092,
      a18_grps: 343.5459446568809,
      p2_imps: 895181924,
      p2_grps: 291.3055398633257,
      index_over_a18: 80,
      index_over_p2: 94,
      at_reach_pct: 6.016642920484688
    },
    {
      service_id: "2074",
      at_real_reach_pct: "21.345916287555802",
      a18_plus_reach_pct: 16.3022207928102,
      display_name: "Freeform",
      p2_plus_reach_pct: 19.073598327587934,
      at_imps: 193322381,
      trps: 552.6047579595407,
      a18_imps: 1196239328,
      a18_grps: 491.8544994038073,
      p2_imps: 1505934241,
      p2_grps: 490.05344646924823,
      index_over_a18: 112,
      index_over_p2: 113,
      at_reach_pct: 15.795978052791293
    }
  ],
  "ATSG Auto Insurance Switchers (Selected Selling Titles)": [
    {
      number_airings: "1344",
      service_id: "106",
      at_imps: 2270673,
      trps: 6.490632574093911,
      a18_imps: 22991808,
      a18_grps: 9.453479708893548,
      p2_imps: 25917696,
      p2_grps: 8.434004555808656,
      index_over_a18: 69,
      index_over_p2: 77,
      display_name: "ESPNU: Lacrosse",
      at_real_reach_pct: "4.25967072107857",
      network_a18_plus_reach_pct: 4.246523038639061,
      network_p2_plus_reach_pct: 3.48214889168403,
      network_at_imps: 72603180,
      network_trps: 207.5334606436601,
      network_a18_imps: 700643926,
      network_a18_grps: 288.08187410057155,
      network_p2_imps: 801315420,
      network_p2_grps: 260.75998047510575,
      network_index_over_a18: 72,
      network_index_over_p2: 80,
      network_at_reach_pct: 3.279946455230499,
      at_reach_pct: 0.3279946455230499,
      a18_plus_reach_pct: 0.12739569115917182,
      p2_plus_reach_pct: 0.09554676836937886
    },
    {
      number_airings: "304",
      service_id: "40",
      at_imps: 4167490,
      trps: 11.912612764314321,
      a18_imps: 38469072,
      a18_grps: 15.817224620698163,
      p2_imps: 42124064,
      p2_grps: 13.707798242759518,
      index_over_a18: 75,
      index_over_p2: 87,
      display_name: "Fully Baked",
      at_real_reach_pct: "29.056846559836195",
      network_a18_plus_reach_pct: 21.7237651744362,
      network_p2_plus_reach_pct: 20.63757691571439,
      network_at_imps: 246453764,
      network_trps: 704.478818055165,
      network_a18_imps: 1883159561,
      network_a18_grps: 774.2936396529748,
      network_p2_imps: 2117412910,
      network_p2_grps: 689.037718841523,
      network_index_over_a18: 91,
      network_index_over_p2: 102,
      network_at_reach_pct: 24.698319575860765,
      at_reach_pct: 3.2107815448618995,
      a18_plus_reach_pct: 2.3896141691879818,
      p2_plus_reach_pct: 2.270133460728583
    },
    {
      number_airings: "260",
      service_id: "2074",
      at_imps: 7155537,
      trps: 20.453834277386225,
      a18_imps: 49068760,
      a18_grps: 20.175469758644795,
      p2_imps: 59107880,
      p2_grps: 19.234585095997403,
      index_over_a18: 101,
      index_over_p2: 106,
      display_name: "Late Night Weekend Acquired Movies",
      at_real_reach_pct: "21.345916287555802",
      network_a18_plus_reach_pct: 16.3022207928102,
      network_p2_plus_reach_pct: 19.073598327587934,
      network_at_imps: 193322381,
      network_trps: 552.6047579595407,
      network_a18_imps: 1196239328,
      network_a18_grps: 491.8544994038073,
      network_p2_imps: 1505934241,
      network_p2_grps: 490.05344646924823,
      network_index_over_a18: 112,
      network_index_over_p2: 113,
      network_at_reach_pct: 20.065161310302454,
      at_reach_pct: 1.8058645179272208,
      a18_plus_reach_pct: 0.16302220792810201,
      p2_plus_reach_pct: 0.13367821050104367
    },
    {
      number_airings: "1092",
      service_id: "50",
      at_imps: 7638971,
      trps: 21.835711672658437,
      a18_imps: 58843512,
      a18_grps: 24.19452818551869,
      p2_imps: 63085932,
      p2_grps: 20.52910250569476,
      index_over_a18: 90,
      index_over_p2: 106,
      display_name: "Late ROS",
      at_real_reach_pct: "6.84575673922262",
      network_a18_plus_reach_pct: 5.1462183653984495,
      network_p2_plus_reach_pct: 4.425747794242667,
      network_at_imps: 32600922,
      network_trps: 93.1885085521366,
      network_a18_imps: 254055256,
      network_a18_grps: 104.45921467044941,
      network_p2_imps: 281787324,
      network_p2_grps: 91.6977949886105,
      network_index_over_a18: 89,
      network_index_over_p2: 102,
      network_at_reach_pct: 6.503468902261489,
      at_reach_pct: 0.9755203353392233,
      a18_plus_reach_pct: 0.514621836539845,
      p2_plus_reach_pct: 0.4631596528858605
    },
    {
      number_airings: "256",
      service_id: "12",
      at_imps: 40339863,
      trps: 115.30998122004108,
      a18_imps: 279135488,
      a18_grps: 114.7713860449817,
      p2_imps: 313699840,
      p2_grps: 102.082603319232,
      index_over_a18: 100,
      index_over_p2: 113,
      display_name: "NBA: Special Edition Game Prm w/ Rpt.",
      at_real_reach_pct: "36.211830760476204",
      network_a18_plus_reach_pct: 18.8891347174103,
      network_p2_plus_reach_pct: 15.11130777392824,
      network_at_imps: 6923314467,
      network_trps: 19790.03412223849,
      network_a18_imps: 47154090357,
      network_a18_grps: 19388.220203527813,
      network_p2_imps: 51544369167,
      network_p2_grps: 16773.30594435405,
      network_index_over_a18: 102,
      network_index_over_p2: 118,
      network_at_reach_pct: 46.35114337340954,
      at_reach_pct: 1.8540457349363817,
      a18_plus_reach_pct: 0.755565388696412,
      p2_plus_reach_pct: 0.7026758114876632
    },
    {
      number_airings: "364",
      service_id: "87",
      at_imps: 4849878,
      trps: 13.863192785524886,
      a18_imps: 34816600,
      a18_grps: 14.315447555610378,
      p2_imps: 36670088,
      p2_grps: 11.932993166287018,
      index_over_a18: 97,
      index_over_p2: 116,
      display_name: "NGC Early Morning",
      at_real_reach_pct: "14.900052937634301",
      network_a18_plus_reach_pct: 11.347775820931401,
      network_p2_plus_reach_pct: 9.30517617316375,
      network_at_imps: 168831153,
      network_trps: 482.5975045053129,
      network_a18_imps: 1293308296,
      network_a18_grps: 531.7660852760989,
      network_p2_imps: 1364652816,
      network_p2_grps: 444.07836511552233,
      network_index_over_a18: 91,
      network_index_over_p2: 109,
      network_at_reach_pct: 10.430037056344013,
      at_reach_pct: 0.5215018528172006,
      a18_plus_reach_pct: 1.5886886149303963,
      p2_plus_reach_pct: 1.255064005795013
    },
    {
      number_airings: "960",
      service_id: "120",
      at_imps: 12995355,
      trps: 37.14673278212431,
      a18_imps: 115407360,
      a18_grps: 47.45173307018625,
      p2_imps: 123953280,
      p2_grps: 40.336244712007804,
      index_over_a18: 78,
      index_over_p2: 92,
      display_name: "ROS Fringe",
      at_real_reach_pct: "5.957072198499691",
      network_a18_plus_reach_pct: 3.86590398914536,
      network_p2_plus_reach_pct: 3.6726087896880926,
      network_at_imps: 96268166,
      network_trps: 275.17893200712507,
      network_a18_imps: 835538092,
      network_a18_grps: 343.5459446568809,
      network_p2_imps: 895181924,
      network_p2_grps: 291.3055398633257,
      network_index_over_a18: 80,
      network_index_over_p2: 94,
      network_at_reach_pct: 6.37406725239467,
      at_reach_pct: 0.2549626900957868,
      a18_plus_reach_pct: 0.1159771196743608,
      p2_plus_reach_pct: 0.09742078052646308
    },
    {
      number_airings: "2730",
      service_id: "63",
      at_imps: 5541194,
      trps: 15.83929574388659,
      a18_imps: 47158020,
      a18_grps: 19.389835944245714,
      p2_imps: 57302700,
      p2_grps: 18.647152619589978,
      index_over_a18: 82,
      index_over_p2: 85,
      display_name: "ROS: Afternoon 12PM-6PM",
      at_real_reach_pct: "4.05048255906519",
      network_a18_plus_reach_pct: 3.53129019093374,
      network_p2_plus_reach_pct: 2.648467643200305,
      network_at_imps: 66369145,
      network_trps: 189.7137046045409,
      network_a18_imps: 432275370,
      network_a18_grps: 177.7374984581226,
      network_p2_imps: 526040640,
      network_p2_grps: 171.181464367068,
      network_index_over_a18: 107,
      network_index_over_p2: 111,
      network_at_reach_pct: 3.9694729078838864,
      at_reach_pct: 0.27786310355187205,
      a18_plus_reach_pct: 0.2118774114560244,
      p2_plus_reach_pct: 0.16526438093569903
    },
    {
      number_airings: "252",
      service_id: "38",
      at_imps: 4977574,
      trps: 14.228208468460068,
      a18_imps: 34779024,
      a18_grps: 14.299997532996176,
      p2_imps: 36414000,
      p2_grps: 11.849658314350798,
      index_over_a18: 99,
      index_over_p2: 120,
      display_name: "Tennis: Australian Open, Total Day",
      at_real_reach_pct: "14.863226701151099",
      network_a18_plus_reach_pct: 12.775536203381698,
      network_p2_plus_reach_pct: 10.60369504880681,
      network_at_imps: 2124053279,
      network_trps: 6071.526443776559,
      network_a18_imps: 15394221152,
      network_a18_grps: 6329.600407877966,
      network_p2_imps: 16828513577,
      network_p2_grps: 5476.249130165961,
      network_index_over_a18: 96,
      network_index_over_p2: 111,
      network_at_reach_pct: 15.01185896816261,
      at_reach_pct: 1.501185896816261,
      a18_plus_reach_pct: 0.3832660861014509,
      p2_plus_reach_pct: 0.3334414949082623
    },
    {
      number_airings: "260",
      service_id: "70",
      at_imps: 9023530,
      trps: 25.79342091888844,
      a18_imps: 53130220,
      a18_grps: 21.845409317051107,
      p2_imps: 62836020,
      p2_grps: 20.447777416205664,
      index_over_a18: 118,
      index_over_p2: 126,
      display_name: "The Simpsons Weekend",
      at_real_reach_pct: "15.233021173347002",
      network_a18_plus_reach_pct: 12.4427352944826,
      network_p2_plus_reach_pct: 10.203042941475733,
      network_at_imps: 134664335,
      network_trps: 384.9329386876759,
      network_a18_imps: 945750295,
      network_a18_grps: 388.8615990296452,
      network_p2_imps: 1081317971,
      network_p2_grps: 351.8769837292548,
      network_index_over_a18: 99,
      network_index_over_p2: 109,
      network_at_reach_pct: 18.2796254080164,
      at_reach_pct: 1.462370032641312,
      a18_plus_reach_pct: 0.24885470588965203,
      p2_plus_reach_pct: 0.20157231177061816
    }
  ],
  "ATSG QSR Influential (Selected Selling Titles)": [
    {
      number_airings: "1344",
      service_id: "106",
      at_imps: 1264127,
      trps: 5.966395071155129,
      a18_imps: 22991808,
      a18_grps: 9.453479708893548,
      p2_imps: 25917696,
      p2_grps: 8.434004555808656,
      index_over_a18: 63,
      index_over_p2: 71,
      display_name: "ESPNU: Lacrosse",
      at_real_reach_pct: "4.25967072107857",
      network_a18_plus_reach_pct: 4.246523038639061,
      network_p2_plus_reach_pct: 3.48214889168403,
      network_at_imps: 40215286,
      network_trps: 189.80710620092418,
      network_a18_imps: 700643926,
      network_a18_grps: 288.08187410057155,
      network_p2_imps: 801315420,
      network_p2_grps: 260.75998047510575,
      network_index_over_a18: 66,
      network_index_over_p2: 73,
      network_at_reach_pct: 4.472654257132499,
      at_reach_pct: 0.49199196828457487,
      a18_plus_reach_pct: 0.08493046077278121,
      p2_plus_reach_pct: 0.07898532851868653
    },
    {
      number_airings: "304",
      service_id: "40",
      at_imps: 3103301,
      trps: 14.646880751017791,
      a18_imps: 38469072,
      a18_grps: 15.817224620698163,
      p2_imps: 42124064,
      p2_grps: 13.707798242759518,
      index_over_a18: 93,
      index_over_p2: 107,
      display_name: "Fully Baked",
      at_real_reach_pct: "29.056846559836195",
      network_a18_plus_reach_pct: 21.7237651744362,
      network_p2_plus_reach_pct: 20.63757691571439,
      network_at_imps: 165617708,
      network_trps: 781.6783438313352,
      network_a18_imps: 1883159561,
      network_a18_grps: 774.2936396529748,
      network_p2_imps: 2117412910,
      network_p2_grps: 689.037718841523,
      network_index_over_a18: 101,
      network_index_over_p2: 113,
      network_at_reach_pct: 21.502066454278783,
      at_reach_pct: 0.4300413290855757,
      a18_plus_reach_pct: 2.3896141691879818,
      p2_plus_reach_pct: 1.9116913353503855
    },
    {
      number_airings: "260",
      service_id: "2074",
      at_imps: 5599899,
      trps: 26.430264309548473,
      a18_imps: 49068760,
      a18_grps: 20.175469758644795,
      p2_imps: 59107880,
      p2_grps: 19.234585095997403,
      index_over_a18: 131,
      index_over_p2: 137,
      display_name: "Late Night Weekend Acquired Movies",
      at_real_reach_pct: "21.345916287555802",
      network_a18_plus_reach_pct: 16.3022207928102,
      network_p2_plus_reach_pct: 19.073598327587934,
      network_at_imps: 127138845,
      network_trps: 600.0667632031349,
      network_a18_imps: 1196239328,
      network_a18_grps: 491.8544994038073,
      network_p2_imps: 1505934241,
      network_p2_grps: 490.05344646924823,
      network_index_over_a18: 122,
      network_index_over_p2: 122,
      network_at_reach_pct: 16.863273867169084,
      at_reach_pct: 2.192225602731981,
      a18_plus_reach_pct: 1.141155455496714,
      p2_plus_reach_pct: 0.8672781461775027
    },
    {
      number_airings: "1092",
      service_id: "50",
      at_imps: 5202328,
      trps: 24.553818377179656,
      a18_imps: 58843512,
      a18_grps: 24.19452818551869,
      p2_imps: 63085932,
      p2_grps: 20.52910250569476,
      index_over_a18: 101,
      index_over_p2: 120,
      display_name: "Late ROS",
      at_real_reach_pct: "6.84575673922262",
      network_a18_plus_reach_pct: 5.1462183653984495,
      network_p2_plus_reach_pct: 4.425747794242667,
      network_at_imps: 21275909,
      network_trps: 100.41750498229663,
      network_a18_imps: 254055256,
      network_a18_grps: 104.45921467044941,
      network_p2_imps: 281787324,
      network_p2_grps: 91.6977949886105,
      network_index_over_a18: 96,
      network_index_over_p2: 110,
      network_at_reach_pct: 6.84575673922262,
      at_reach_pct: 0.6161181065300357,
      a18_plus_reach_pct: 0.15438655096195347,
      p2_plus_reach_pct: 0.1466672234138558
    },
    {
      number_airings: "256",
      service_id: "12",
      at_imps: 27149046,
      trps: 128.1373945614382,
      a18_imps: 279135488,
      a18_grps: 114.7713860449817,
      p2_imps: 313699840,
      p2_grps: 102.082603319232,
      index_over_a18: 112,
      index_over_p2: 126,
      display_name: "NBA: Special Edition Game Prm w/ Rpt.",
      at_real_reach_pct: "36.211830760476204",
      network_a18_plus_reach_pct: 18.8891347174103,
      network_p2_plus_reach_pct: 15.11130777392824,
      network_at_imps: 4501219429,
      network_trps: 21244.743606743566,
      network_a18_imps: 47154090357,
      network_a18_grps: 19388.220203527813,
      network_p2_imps: 51544369167,
      network_p2_grps: 16773.30594435405,
      network_index_over_a18: 110,
      network_index_over_p2: 127,
      network_at_reach_pct: 33.31488429963811,
      at_reach_pct: 3.331488429963811,
      a18_plus_reach_pct: 1.88891347174103,
      p2_plus_reach_pct: 1.6055764509798756
    },
    {
      number_airings: "364",
      service_id: "87",
      at_imps: 2714862,
      trps: 12.813536925012508,
      a18_imps: 34816600,
      a18_grps: 14.315447555610378,
      p2_imps: 36670088,
      p2_grps: 11.932993166287018,
      index_over_a18: 90,
      index_over_p2: 107,
      display_name: "NGC Early Morning",
      at_real_reach_pct: "14.900052937634301",
      network_a18_plus_reach_pct: 11.347775820931401,
      network_p2_plus_reach_pct: 9.30517617316375,
      network_at_imps: 90765617,
      network_trps: 428.39330265488957,
      network_a18_imps: 1293308296,
      network_a18_grps: 531.7660852760989,
      network_p2_imps: 1364652816,
      network_p2_grps: 444.07836511552233,
      network_index_over_a18: 81,
      network_index_over_p2: 96,
      network_at_reach_pct: 18.32706511329019,
      at_reach_pct: 1.649435860196117,
      a18_plus_reach_pct: 0.22695551641862804,
      p2_plus_reach_pct: 0.2110686302693241
    },
    {
      number_airings: "960",
      service_id: "120",
      at_imps: 10070422,
      trps: 47.530128815114686,
      a18_imps: 115407360,
      a18_grps: 47.45173307018625,
      p2_imps: 123953280,
      p2_grps: 40.336244712007804,
      index_over_a18: 100,
      index_over_p2: 118,
      display_name: "ROS Fringe",
      at_real_reach_pct: "5.957072198499691",
      network_a18_plus_reach_pct: 3.86590398914536,
      network_p2_plus_reach_pct: 3.6726087896880926,
      network_at_imps: 69331922,
      network_trps: 327.23108364443317,
      network_a18_imps: 835538092,
      network_a18_grps: 343.5459446568809,
      network_p2_imps: 895181924,
      network_p2_grps: 291.3055398633257,
      network_index_over_a18: 95,
      network_index_over_p2: 112,
      network_at_reach_pct: 6.791062306289648,
      at_reach_pct: 0.8828380998176543,
      a18_plus_reach_pct: 0.1159771196743608,
      p2_plus_reach_pct: 0.10901849249389917
    },
    {
      number_airings: "2730",
      service_id: "63",
      at_imps: 3659619,
      trps: 17.272580455559385,
      a18_imps: 47158020,
      a18_grps: 19.389835944245714,
      p2_imps: 57302700,
      p2_grps: 18.647152619589978,
      index_over_a18: 89,
      index_over_p2: 93,
      display_name: "ROS: Afternoon 12PM-6PM",
      at_real_reach_pct: "4.05048255906519",
      network_a18_plus_reach_pct: 3.53129019093374,
      network_p2_plus_reach_pct: 2.648467643200305,
      network_at_imps: 44718702,
      network_trps: 211.0622199171179,
      network_a18_imps: 432275370,
      network_a18_grps: 177.7374984581226,
      network_p2_imps: 526040640,
      network_p2_grps: 171.181464367068,
      network_index_over_a18: 119,
      network_index_over_p2: 123,
      network_at_reach_pct: 5.022598373240836,
      at_reach_pct: 0.10045196746481672,
      a18_plus_reach_pct: 0.14125160763734962,
      p2_plus_reach_pct: 0.12006386649174718
    },
    {
      number_airings: "252",
      service_id: "38",
      at_imps: 3343742,
      trps: 15.781710442129306,
      a18_imps: 34779024,
      a18_grps: 14.299997532996176,
      p2_imps: 36414000,
      p2_grps: 11.849658314350798,
      index_over_a18: 110,
      index_over_p2: 133,
      display_name: "Tennis: Australian Open, Total Day",
      at_real_reach_pct: "14.863226701151099",
      network_a18_plus_reach_pct: 12.775536203381698,
      network_p2_plus_reach_pct: 10.60369504880681,
      network_at_imps: 1373304557,
      network_trps: 6481.688719157741,
      network_a18_imps: 15394221152,
      network_a18_grps: 6329.600407877966,
      network_p2_imps: 16828513577,
      network_p2_grps: 5476.249130165961,
      network_index_over_a18: 102,
      network_index_over_p2: 118,
      network_at_reach_pct: 15.01185896816261,
      at_reach_pct: 1.3510673071346349,
      a18_plus_reach_pct: 0.7665321722029018,
      p2_plus_reach_pct: 0.7282055635927568
    },
    {
      number_airings: "260",
      service_id: "70",
      at_imps: 4293460,
      trps: 20.264167471794792,
      a18_imps: 53130220,
      a18_grps: 21.845409317051107,
      p2_imps: 62836020,
      p2_grps: 20.447777416205664,
      index_over_a18: 93,
      index_over_p2: 99,
      display_name: "The Simpsons Weekend",
      at_real_reach_pct: "15.233021173347002",
      network_a18_plus_reach_pct: 12.4427352944826,
      network_p2_plus_reach_pct: 10.203042941475733,
      network_at_imps: 81790755,
      network_trps: 386.03397249418816,
      network_a18_imps: 945750295,
      network_a18_grps: 388.8615990296452,
      network_p2_imps: 1081317971,
      network_p2_grps: 351.8769837292548,
      network_index_over_a18: 99,
      network_index_over_p2: 110,
      network_at_reach_pct: 19.498267101884164,
      at_reach_pct: 0.19498267101884165,
      a18_plus_reach_pct: 1.24427352944826,
      p2_plus_reach_pct: 1.1945025882703297
    }
  ]
};

const pricing = [
  {
    network: "ABC",
    min_cpm: 0.68,
    max_cpm: 69.98,
    avg_cpm: 11.57,
    median_cpm: 7.33,
    airings: 1095
  },
  {
    network: "ESPNews",
    service_id: "63",
    min_cpm: 12.14,
    max_cpm: 19.82,
    avg_cpm: 15.56,
    median_cpm: 13.38,
    airings: 3024
  },
  {
    network: "ESPN",
    service_id: "12",
    min_cpm: 0.0,
    max_cpm: 83.85,
    avg_cpm: 26.52,
    median_cpm: 27.14,
    airings: 21584
  },
  {
    network: "ESPN2",
    service_id: "38",
    min_cpm: 0.0,
    max_cpm: 153.59,
    avg_cpm: 18.74,
    median_cpm: 14.67,
    airings: 18095
  },
  {
    network: "ESPNU",
    service_id: "106",
    min_cpm: 0.0,
    max_cpm: 24.15,
    avg_cpm: 15.18,
    median_cpm: 13.28,
    airings: 6012
  },
  {
    network: "Freeform",
    service_id: "2074",
    min_cpm: 5.88,
    max_cpm: 36.43,
    avg_cpm: 11.64,
    median_cpm: 9.29,
    airings: 1436
  },
  {
    network: "FX",
    service_id: "40",
    min_cpm: 4.23,
    max_cpm: 11.34,
    avg_cpm: 8.69,
    median_cpm: 9.48,
    airings: 1439
  },
  {
    network: "FX Movie Channel",
    service_id: "50",
    min_cpm: 3.01,
    max_cpm: 4.44,
    avg_cpm: 3.58,
    median_cpm: 3.01,
    airings: 182
  },
  {
    network: "FXX",
    service_id: "70",
    min_cpm: 2.0,
    max_cpm: 10.17,
    avg_cpm: 6.26,
    median_cpm: 4.86,
    airings: 1074
  },
  {
    network: "Nat Geo Wild",
    service_id: "120",
    min_cpm: 1.53,
    max_cpm: 2.39,
    avg_cpm: 2.07,
    median_cpm: 2.26,
    airings: 1180
  },
  {
    network: "National Geographic",
    service_id: "87",
    min_cpm: 2.8,
    max_cpm: 14.12,
    avg_cpm: 4.62,
    median_cpm: 4.16,
    airings: 1550
  }
];
